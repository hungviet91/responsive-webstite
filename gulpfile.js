var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch'); //watching file
var concat = require('gulp-concat'); //concatenating file
var sourcemaps = require('gulp-sourcemaps'); //show the source of scss rules
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber'); //notify gulp when error occurs
var cleanCss = require('gulp-clean-css'); // minify CSS
var minify = require('gulp-minify'); // minify JS
var wait = require('gulp-wait');


gulp.task('sass', function() {
    return gulp.src('assets/scss/**/*.scss')
        .pipe(wait(300))
        .pipe(plumber(function(error) {
            console.log(error.toString());
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(concat('app.min.css'))
        .pipe(cleanCss())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    return gulp.src('assets/js/*.js')
        .pipe(plumber(function(error) {
            console.log(error.toString());
            this.emit('end');
        }))
        .pipe(concat('app.min.js'))
        .pipe(minify())
        .pipe(gulp.dest('dist/js/'))
        .pipe(browserSync.stream());
});

/*Browsersync*/
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    })
})

gulp.task('watch', ['sass', 'js', 'browserSync'], function() {
    gulp.watch('assets/scss/**/*.scss', ['sass']);
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('assets/js/*.js', ['js']);
});

gulp.task('default', ['watch']);
